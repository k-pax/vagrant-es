package io.kpax.es.test;

import static io.kpax.es.test.ESUtils.KEYSTORE;
import static io.kpax.es.test.ESUtils.PORT;
import static io.kpax.es.test.ESUtils.SEARCH_NODES;
import static io.kpax.es.test.ESUtils.TRUSTSTORE;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServlet;

import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;

import com.floragunn.searchguard.ssl.SearchGuardSSLPlugin;

public abstract class ESInitializer extends HttpServlet {
	private static final long serialVersionUID = 663440031009329078L;

	private Settings settings = Settings.builder().put("path.home", ".").put("cluster.name", "elasticsearch")
			.put("searchguard.ssl.transport.enabled", true).put("searchguard.ssl.transport.keystore_filepath", KEYSTORE)
			.put("searchguard.ssl.transport.truststore_filepath", TRUSTSTORE)
			.put("searchguard.ssl.transport.truststore_password", "changeit")
			.put("searchguard.ssl.transport.keystore_password", "changeit")
			.put("searchguard.ssl.transport.enforce_hostname_verification", false)
			.build();

	protected Settings getSettings() {
		return settings;
	}

	protected TransportClient createTransportClient() throws UnknownHostException {
		TransportClient transportClient = new PreBuiltTransportClient(settings, SearchGuardSSLPlugin.class);

		for (String nodeHost : SEARCH_NODES) {
			transportClient.addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName(nodeHost), PORT));
		}

		return transportClient;
	}

	protected List<Task> generateTasks() {
		Task task0 = new Task();
		task0.setAssignee("Kostas Perperidis");
		task0.setDescription("A general Description of a general Task made up for Kostas");

		Task task1 = new Task();
		task1.setAssignee("Scooby Doo");
		task1.setDescription("Solve the mystery");

		List<Task> taskList = new ArrayList<>();
		taskList.add(task0);
		taskList.add(task1);

		return taskList;
	}

	protected List<WorkPackage> generateWorkPackages() {
		WorkPackage wp0 = new WorkPackage();
		wp0.setType("General Type");
		wp0.setLanguage("English");
		wp0.setAbstractDescription("This is the most general work package made up for just an example");

		List<WorkPackage> wp = new ArrayList<>();
		wp.add(wp0);

		return wp;
	}

	protected List<Text> generateTexts() {
		Text t0 = new Text();
		t0.setContent("This is a text with a general description for a general example made up by Perperidis on a sunday evening");
		t0.setTitle("An example, a quite general one");

		List<Text> t = new ArrayList<>();
		t.add(t0);

		return t;
	}
}
