package io.kpax.es.test;

public class ESUtils {

	public static final String[] SEARCH_NODES = { 
			"192.168.1.21", 
			"192.168.1.22", 
			"192.168.1.23" };

	public static final String KEYSTORE = "/home/vagrant/kirk-keystore.jks";

	public static final String TRUSTSTORE = "/home/vagrant/truststore.jks";

	public static final int PORT = 9300;
	
	public static final String INDEX_NAME = "estest";
	
	public static final String OP_MON_CREATE = "create";
	
	public static final String OP_MON_DELETE = "delete";
	
	public static final String OP_MON_LIST = "list";
	
	public static final String OP_MON_DELETE_ALL = "deleteAll";
	
	public static final String OP_Q_GENERATE_DATA = "generate";
	
	public static final String OP_Q_QUERY_DATA = "query";
	
	public static final String PARM_INDEX_NAME = "indexName";
	
	public static final String PARM_DATA_TYPE = "dataType";
	
	public static final String PARM_QUERY_TERM = "term";
	
	public static final String TASK_DATA_TYPE = "task";
	
	public static final String WP_DATA_TYPE = "workpackage";
	
	public static final String TEXT_DATA_TYPE = "text";
	
	public static final String[] DATA_TYPES = { TASK_DATA_TYPE, WP_DATA_TYPE, TEXT_DATA_TYPE };
}
