package io.kpax.es.test;

import static io.kpax.es.test.ESUtils.*;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.cluster.metadata.IndexMetaData;
import org.elasticsearch.common.collect.ImmutableOpenMap;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;

import com.google.gson.Gson;

public class ESQueryServlet extends ESInitializer {
	TransportClient transportClient = null;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String op = request.getParameter("op");
		String parmIndexName = request.getParameter(PARM_INDEX_NAME);
		String parmDataType = request.getParameter(PARM_DATA_TYPE);
		String parmQueryTerm = request.getParameter(PARM_QUERY_TERM);

		if (op != null) {
			if (OP_Q_GENERATE_DATA.equals(op) && parmIndexName != null && parmDataType != null
					&& Arrays.asList(DATA_TYPES).contains(parmDataType)) {

				generateData(response, parmIndexName, parmDataType);

			} else if (OP_Q_QUERY_DATA.equals(op) && parmQueryTerm != null) {
				queryData(response, parmQueryTerm);
			} else {
				response.getWriter().println(
						"Syntax is ?op=generate|query&indexName=<i>something</i>&dataType=<i>task|workpackage|text</i>");
			}
		}
	}

	private void queryData(HttpServletResponse response, String parmQueryTerm) throws IOException {
		if (transportClient != null) {
			String[] availableIndices = getIndices();
			if (availableIndices.length > 0) {
				SearchResponse ser = transportClient.prepareSearch(availableIndices)
						.setQuery(QueryBuilders.matchQuery("_all", parmQueryTerm)).highlighter(new HighlightBuilder().field("_all").preTags("<b>").postTags("</b>"))
						.get();

				StringBuilder sb = new StringBuilder();
				if (ser.getHits() != null || ser.getHits().getHits() != null || ser.getHits().getHits().length > 0) {
					SearchHit[] searchHits = ser.getHits().getHits();
					for (SearchHit searchHit : searchHits) {
						response.getWriter().println(searchHit.getIndex());
						response.getWriter().println("----------------------------");
//						response.getWriter().println(searchHit.getSourceAsString());
						for (Entry<String, Object> entry : searchHit.getSource().entrySet()) {
							response.getWriter().println(entry.getKey() + " : " + entry.getValue());
						}
						response.getWriter().println(searchHit.getHighlightFields().isEmpty());
						for (Entry<String, HighlightField> entry : searchHit.getHighlightFields().entrySet()) {
							for (Text text : entry.getValue().fragments()) {
								sb.append(text.string());
							}
							sb.append("\n");
						}
					}
					response.getWriter().println(sb.toString());

				} else {
					response.getWriter().println("there aren't any results for " + parmQueryTerm);
				}
			} else {
				response.getWriter().println("There aren't any indices available");
			}
		}
	}

	private String[] getIndices() {
		String[] indices = {};
		if (transportClient != null) {
			ImmutableOpenMap<String, IndexMetaData> list = transportClient.admin().cluster().prepareState().get()
					.getState().getMetaData().getIndices();
			if (!list.isEmpty()) {
				List<String> indicesList = new ArrayList<>();
				list.forEach(entry -> indicesList.add(entry.key));
				indices = indicesList.toArray(indices);
			}
		}
		return indices;
	}

	private void generateData(HttpServletResponse response, String parmIndexName, String parmDataType) {
		if (TASK_DATA_TYPE.equals(parmDataType)) {
			generateTasks().forEach(task -> {
				IndexRequest ireq = new IndexRequest(parmIndexName, parmDataType);
				ireq.source(new Gson().toJson(task));
				transportClient.index(ireq).actionGet();
			});
		}

		if (WP_DATA_TYPE.equals(parmDataType)) {
			generateWorkPackages().forEach(wp -> {
				IndexRequest ireq = new IndexRequest(parmIndexName, parmDataType);
				ireq.source(new Gson().toJson(wp));
				transportClient.index(ireq).actionGet();
			});
		}

		if (TEXT_DATA_TYPE.equals(parmDataType)) {
			generateTexts().forEach(t -> {
				IndexRequest ireq = new IndexRequest(parmIndexName, parmDataType);
				ireq.source(new Gson().toJson(t));
				transportClient.index(ireq).actionGet();
			});
		}
	}

	@Override
	public void init() throws ServletException {
		try {
			transportClient = createTransportClient();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void destroy() {
		if (transportClient != null) {
			transportClient.close();
		}

		System.out.println("Servlet " + this.getServletName() + " has stopped");
	}

}
