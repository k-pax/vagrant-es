package io.kpax.es.test;

import org.elasticsearch.action.admin.cluster.health.ClusterHealthResponse;
import org.elasticsearch.action.admin.indices.create.CreateIndexResponse;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexResponse;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.cluster.metadata.IndexMetaData;
import org.elasticsearch.common.collect.ImmutableOpenMap;

import java.io.IOException;
import java.net.UnknownHostException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static io.kpax.es.test.ESUtils.*;

public class ESMonitorServlet extends ESInitializer {
	TransportClient transportClient = null;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		if (transportClient != null) {
			ClusterHealthResponse healthResponse = transportClient.admin().cluster().prepareHealth().execute()
					.actionGet();

			response.getWriter().println("Status is " + healthResponse.getStatus().name());
			response.getWriter().println("Number of nodes " + healthResponse.getNumberOfNodes());
		} else {
			response.getWriter().println("Transport Client could not be initialized - please check logs!");
		}

		if (request.getParameter("op") != null) {
			String op = request.getParameter("op");
			String indexName = request.getParameter(PARM_INDEX_NAME);
			if (OP_MON_CREATE.equals(op)) {
				createIndex(response, indexName);
			} else if (OP_MON_DELETE.equals(op)) {
				dropIndex(response, indexName);
			} else if (OP_MON_LIST.equals(op)) {
				listIndices(response);
			} else if (OP_MON_DELETE_ALL.equals(op)) {
				deleteAll(response);
			}
		}
	}

	private ImmutableOpenMap<String, IndexMetaData> getIndices(HttpServletResponse response) throws IOException {
		if (transportClient != null) {
			ImmutableOpenMap<String, IndexMetaData> list =  transportClient
					.admin()
					.cluster()
					.prepareState()
					.get()
					.getState()
					.getMetaData()
					.getIndices();
			if (!list.isEmpty()) {
				return list;
			}
			response.getWriter().println("There aren't any indices at this time available");
			return null;
		}
		return null;
	}
	
	private void deleteAll(HttpServletResponse response) throws IOException {
		if (transportClient != null) {
			ImmutableOpenMap<String, IndexMetaData> indexMap = getIndices(response);
			if (indexMap != null) {
				indexMap.forEach(entry -> {
					try {
						dropIndex(response, entry.key);
					} catch (IOException e) {
						e.printStackTrace();
					}
				});
			}
		}
	}
	
	private void listIndices(HttpServletResponse response) throws IOException {
		if (transportClient != null) {
			ImmutableOpenMap<String, IndexMetaData> indexMap = getIndices(response);
			
			if (indexMap != null) {
				indexMap.forEach(entry -> {
					try {
						response.getWriter().println(entry.key);
					} catch (IOException e) {
						e.printStackTrace();
					}
				});
			}
		}
	}
	
	private void createIndex(HttpServletResponse response, String indexName) throws IOException {
		if (transportClient != null) {
			CreateIndexResponse clientResponse = transportClient.admin().indices().prepareCreate(
					indexName != null ? indexName : INDEX_NAME
			).get();

			if (clientResponse.isAcknowledged()) {
				response.getWriter().println(
						indexName != null ? "Index Created with custom name " + indexName : "Index Created with default name " + INDEX_NAME
				);
			}
		}		
	}
	
	private void dropIndex(HttpServletResponse response, String indexName) throws IOException {
		if (transportClient != null) {
			DeleteIndexResponse clientResponse = transportClient.admin().indices().prepareDelete(
					indexName != null ? indexName : INDEX_NAME
				).get();

			if (clientResponse.isAcknowledged()) {
				response.getWriter().println(
						indexName != null ? "Custom index " + indexName + " dropped" : "Default index " + INDEX_NAME + " dropped" 
				);
			}
		}		
	}

	@Override
	public void init() throws ServletException {
		try {
			transportClient = createTransportClient();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		System.out.println("Servlet " + this.getServletName() + " has started");
	}

	@Override
	public void destroy() {
		if (transportClient != null) {
			transportClient.close();
		}

		System.out.println("Servlet " + this.getServletName() + " has stopped");
	}
}
