#!/bin/bash

echo 'cluster.name: figaro
discovery.zen.ping.unicast.hosts: ["192.168.33.5", "192.168.33.7", "192.168.33.6"]
network.host: 192.168.33.6
discovery.zen.minimum_master_nodes: 2
searchguard.ssl.http.enabled: true
searchguard.ssl.http.keystore_filepath: search2-keystore.jks
searchguard.ssl.http.keystore_password: 183740162df9dfcb4849
searchguard.ssl.transport.keystore_filepath: search2-keystore.jks
searchguard.ssl.transport.keystore_password: 183740162df9dfcb4849
searchguard.ssl.transport.truststore_filepath: truststore.jks
searchguard.ssl.transport.truststore_password: 9dfc0a197c191d8eb8f8
searchguard.ssl.transport.enforce_hostname_verification: false' >> /usr/share/elastic/config/elasticsearch.yml
