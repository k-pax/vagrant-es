#!/bin/bash

sudo add-apt-repository -y ppa:webupd8team/java
sudo apt-get update
echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | sudo /usr/bin/debconf-set-selections
sudo apt-get -y install oracle-java8-installer

wget https://download.elastic.co/elasticsearch/release/org/elasticsearch/distribution/tar/elasticsearch/2.4.4/elasticsearch-2.4.4.tar.gz

sudo tar -xzf elasticsearch-2.4.4.tar.gz -C /usr/share
sudo ln -s /usr/share/elasticsearch-2.4.4/ /usr/share/elastic
sudo chown -R vagrant:vagrant /usr/share/elasticsearch-2.4.4/