#!/bin/bash
sudo apt-get update -y
sudo apt-get install apache2 -y
sudo a2enmod proxy_http

sudo add-apt-repository -y ppa:webupd8team/java
sudo apt-get update
echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | sudo /usr/bin/debconf-set-selections
sudo apt-get -y install oracle-java8-installer

wget http://apache.otenet.gr/dist/tomcat/tomcat-8/v8.0.42/bin/apache-tomcat-8.0.42.tar.gz
sudo tar -xzf apache-tomcat-8.0.42.tar.gz -C /usr/share
sudo ln -s /usr/share/apache-tomcat-8.0.42 /usr/share/tomcat
sudo chown -R vagrant:vagrant /usr/share/apache-tomcat-8.0.42

/usr/share/tomcat/bin/startup.sh

sudo cp /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-enabled/appserver.conf

sudo chmod o+w /etc/apache2/sites-enabled/appserver.conf

sudo echo '<VirtualHost *:80> 
        ServerAdmin webmaster@localhost 
	        DocumentRoot /var/www/html 
		        ErrorLog ${APACHE_LOG_DIR}/error.log 
			        CustomLog ${APACHE_LOG_DIR}/access.log combined 
				        ProxyPass "/" "http://localhost:8080/" 
					        ProxyPassReverse "/" "http://localhost:8080" 
						</VirtualHost>' > /etc/apache2/sites-enabled/appserver.conf

sudo rm /etc/apache2/sites-enabled/000-default.conf

sudo service apache2 restart

